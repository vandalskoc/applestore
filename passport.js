var express = require('express')
var thanhVien = require('./Database/models/ThanhVien');
var LocalStrategy = require('passport-local').Strategy;
var bcrypt = require('bcryptjs');
var credentials = require('./credentials')
var GoogleStrategy = require('passport-google-oauth20').Strategy
var config = credentials.authProviders
var app = express();
var env = app.get('env')

module.exports = function(passport) {
    passport.use('login-account', new LocalStrategy(function(email, password, done) {
        let notify = 'Thông tin đăng nhập không chính xác!'
        thanhVien.findOne({email: email},(err, email)=> {
            if(err) done(null,false, {message: notify});
            if(!email) {
                return done(null,false,{message: notify});
            }
            if(!email.password) {
                return done(null,false,{message: notify});
            }
            bcrypt.compare(password, email.password,(err,isMatch)=> {
                if(err) return done(err);
                if(isMatch) {
                    return done(null, email);
                } else {
                    return done(null, false, {message: notify});
                }
            });
        });
    }));

    passport.use('login-google', new GoogleStrategy({
        clientID        : config.gooogle[env].appId,
        clientSecret    : config.gooogle[env].appSecret,
        callbackURL     : config.gooogle[env].callbackURL
    },
    function(accessToken, refreshToken, profile, done) {
        process.nextTick(function() {
            thanhVien.findOne({ 'uid' : profile.id }, function(err, user) {
                if (err)
                    return done(err);
                if (user) {
                    return done(null, user); 
                } else {
                    var newUser = new thanhVien();
                    newUser.uid = profile.id;                                
                    newUser.hoten  = profile.name.givenName + ' ' + profile.name.familyName; 
                    newUser.hinhanhtv = profile.photos[0].value;
                    newUser.role = 'Customer';
                    newUser.email = profile.emails[0].value; 
                    newUser.save(function(err) {
                        if (err)
                            return done(null,false, {message: 'Email đã được đăng ký tài khoản!'});
                        return done(null, newUser);
                    });
                }
    
            });
    
        })
    
    }));
    
    passport.serializeUser((user, done) => {
        done(null, user);
    })
    
    passport.deserializeUser((user, done) => {
        done(null, user);
    })
}