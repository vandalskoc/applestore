var express = require('express');
var loaiSanPham = require('./../../Database/models/LoaiSanPham');
var bcrypt = require('bcryptjs');
var saltRounds = 10;
var thanhVien = require('../../Database/models/ThanhVien');
var sanPham = require('./../../Database/models/SanPham');
var gioHang = require('./../../Database/models/GioHang');
var donHang = require('./../../Database/models/DonHang');
var thongTinTV = require('../../Database/models/ThongTinThanhVien');
var nhanXet = require('./../../Database/models/NhanXet');
const thongtinthanhvien = require('../../Database/models/ThongTinThanhVien');
var router = express.Router();

/* GET home page. */
router.get('/', async(req, res) => {
    const macBook = await loaiSanPham.find({ 'tendongsp': 'MACBOOK' });
    const iMac = await loaiSanPham.find({ 'tendongsp': 'IMAC' });
    const phuKien = await loaiSanPham.find({ 'tendongsp': 'PHỤ KIỆN' });
    res.render('Client/home', {
        macBook: macBook,
        iMac: iMac,
        phuKien: phuKien,
        user: req.user
    });
});

router.get('/signup', async(req, res) => {
    const macBook = await loaiSanPham.find({ 'tendongsp': 'MACBOOK' });
    const iMac = await loaiSanPham.find({ 'tendongsp': 'IMAC' });
    const phuKien = await loaiSanPham.find({ 'tendongsp': 'PHỤ KIỆN' });
    res.render('Client/signup', {
        macBook: macBook,
        iMac: iMac,
        phuKien: phuKien,
        user: req.user,
        message: req.flash('message')
    });
});

router.post('/signup', async(req, res) => {
    thanhVien.findOne({ 'email': req.body.email }, function(err, user) {
        if (user) {
            req.flash('message', 'Email đã được sử dụng')
            res.redirect('/signup');
        } else {
            bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
                thanhVien.create({
                    hoten: req.body.hoten,
                    email: req.body.email,
                    password: hash
                }).then(function(data) {
                    if (data) {
                        req.flash('message', 'Đăng ký tài khoản thành công!')
                        res.redirect('/login');
                    }
                });
            });
        }
    });
})


router.get('/products', async(req, res) => {
    try {
        const macBook = await loaiSanPham.find({ 'tendongsp': 'MACBOOK' });
        const iMac = await loaiSanPham.find({ 'tendongsp': 'IMAC' });
        const phuKien = await loaiSanPham.find({ 'tendongsp': 'PHỤ KIỆN' });
        const sanPhamVal = await sanPham.find({ 'idloaisp': req.query.id });
        const loaiSp = await loaiSanPham.findById(req.query.id);
        const dongSp = await loaiSanPham.find({ 'tendongsp': loaiSp.tendongsp })
        res.render('Client/products', {
            macBook: macBook,
            iMac: iMac,
            phuKien: phuKien,
            user: req.user,
            sanPham: sanPhamVal,
            loaiSp: loaiSp,
            dongSp: dongSp
        });
    } catch (e) {
        res.send('Đường dẫn không hợp lệ')
    }
});

router.get('/profile', isLoggedIn, async(req, res) => {
    const macBook = await loaiSanPham.find({ 'tendongsp': 'MACBOOK' });
    const iMac = await loaiSanPham.find({ 'tendongsp': 'IMAC' });
    const phuKien = await loaiSanPham.find({ 'tendongsp': 'PHỤ KIỆN' });
    const thanhvien = await thanhVien.findById(req.user._id).populate('idthongtin')
    res.render('Client/profile', {
        macBook: macBook,
        iMac: iMac,
        phuKien: phuKien,
        user: req.user,
        thanhvien: thanhvien,
        message: req.flash('message')
    });

});


router.get('/detailProd', async(req, res) => {
    try {
        const macBook = await loaiSanPham.find({ 'tendongsp': 'MACBOOK' });
        const iMac = await loaiSanPham.find({ 'tendongsp': 'IMAC' });
        const phuKien = await loaiSanPham.find({ 'tendongsp': 'PHỤ KIỆN' });
        const sanPhamVal = await sanPham.findById(req.query.id).populate('idloaisp')
        const sanPhamLienQuan = await sanPham.find({ 'idloaisp': sanPhamVal.idloaisp._id, '_id': { $ne: req.query.id } }).limit(2);
        const nhanXetVal = await nhanXet.find({'idsanpham': req.query.id}).populate('idthanhvien').sort({'_id': -1})
        res.render('Client/detailProd', {
            macBook: macBook,
            iMac: iMac,
            phuKien: phuKien,
            user: req.user,
            sanPham: sanPhamVal,
            sanPhamLienQuan: sanPhamLienQuan,
            nhanXet: nhanXetVal,
            message: req.flash('success')
        });
    } catch (e) {
        res.send('Đường dẫn không hơp lệ')
    }

});

router.get('/cart', isLoggedIn, async(req, res) => {
	try {
		const macBook = await loaiSanPham.find({'tendongsp': 'MACBOOK'});
		const iMac = await loaiSanPham.find({'tendongsp': 'IMAC'});
		const phuKien = await loaiSanPham.find({'tendongsp': 'PHỤ KIỆN'});
		const gioHangVal = await gioHang.find({'idthanhvien': req.user._id}).populate('idsanpham')
		res.render('Client/cart',{
			macBook: macBook,
			iMac: iMac,
			phuKien: phuKien,
			user: req.user,
			gioHang: gioHangVal,
            message: req.flash('success'),
            messageError: req.flash('error')
		});
	} catch (e) {
		res.send('Đường dẫn không hơp lệ')
	}
	
});

router.post('/cartnumber', async(req, res) => {
	let idSanPham = req.body.idsanpham 
	let soLuong = req.body.soluong
	let tongtien = parseInt(req.body.tongtien)
	let op = req.body.op
	let check = req.body.check
	const data = await gioHang.findByIdAndUpdate({ _id: idSanPham }, {$set: {soluong: soLuong}}).populate('idsanpham')
	if(check >= 2 && check <= 100){
		if(op === '-'){
			tongtien = tongtien - data.idsanpham.giatien
		}else {
			tongtien = tongtien + data.idsanpham.giatien
		}
	}
	giaTien = soLuong * data.idsanpham.giatien
	res.send({data, giaTien, tongtien})
})

router.post('/cart', async(req, res) => {
    const user = await thanhVien.findById(req.user._id)
    if(req.body.thanhtien != 0) {
        if(!user.idthongtin){
            req.flash('error', 'Vui lòng cập nhật thông tin cá nhân trước khi đặt hàng!')
            backURL = req.header('Referer') || '/';
            res.redirect(backURL);
        }else {
            let newDonHang = new donHang({
                idsanpham: req.body.idsanpham,
                idthanhvien: req.user._id,
                soluong: req.body.soluong, 
                thanhtien: req.body.thanhtien,
                thoigian: Date.now()
            });
            newDonHang.save(function(err) {
                if (err) {
                    res.json({ kq: false, errMsg: err });
                } else {
                    gioHang.deleteMany({ idthanhvien: req.user._id }, function(err, data){
                        if(err){
                            res.json({kq:false, errMsg:err});
                        }else{
                            res.redirect('/odermange');
                        }
                    })	
                }
            })
        }
    }else {
        req.flash('error', 'Giỏ hàng trống!')
        backURL = req.header('Referer') || '/';
        res.redirect(backURL);
    }
})

router.get('/deletecart/:id', (req, res) => {
    gioHang.findByIdAndDelete(req.params.id, function(err, data) {
        if (err) {
            res.json({ kq: false, errMsg: err });
        } else {
            req.flash('success', 'Xóa sản phẩm thành công!')
            backURL = req.header('Referer') || '/';
            res.redirect(backURL);
        }
    })
})

router.get('/deleteoder/:id', (req, res) => {
    donHang.findByIdAndDelete(req.params.id, function(err, data) {
        if (err) {
            res.json({ kq: false, errMsg: err });
        } else {
            req.flash('success', 'Xóa đơn hàng thành công!')
            backURL = req.header('Referer') || '/';
            res.redirect(backURL);
        }
    })
})


router.get('/oder/:id', isLoggedIn, async(req, res) => {
	try {
		let idsanpham = req.params.id
		const macBook = await loaiSanPham.find({'tendongsp': 'MACBOOK'});
		const iMac = await loaiSanPham.find({'tendongsp': 'IMAC'});
		const phuKien = await loaiSanPham.find({'tendongsp': 'PHỤ KIỆN'});
		const chiTietDonHang = await donHang.findById(idsanpham).populate('idsanpham')
		
		res.render('Client/oder',{
			macBook: macBook,
			iMac: iMac,
			phuKien: phuKien,
			user: req.user,
			chiTietDonHang: chiTietDonHang
			
		});
	} catch (e) {
		res.send('Đường dẫn không hơp lệ')
	}
	
});

router.get('/odermange', isLoggedIn, async(req, res) => {
	try {
		const macBook = await loaiSanPham.find({'tendongsp': 'MACBOOK'});
		const iMac = await loaiSanPham.find({'tendongsp': 'IMAC'});
		const phuKien = await loaiSanPham.find({'tendongsp': 'PHỤ KIỆN'});
		const donHangVal = await donHang.find({'idthanhvien': req.user._id}).populate('idsanpham')
		let count = 0
		let i = 0
		donHangVal.forEach(data => {
			count += data.soluong[i]
			i++
		})
		res.render('Client/allorder',{
			macBook: macBook,
			iMac: iMac,
			phuKien: phuKien,
			user: req.user,
			donHang: donHangVal,
            message: req.flash('success')
		});
	} catch (e) {
		res.send('Đường dẫn không hơp lệ')
	}
	
});


router.get('/search', async(req, res) => {
    try {
        const search = req.query.search || ''
        const macBook = await loaiSanPham.find({ 'tendongsp': 'MACBOOK' });
        const iMac = await loaiSanPham.find({ 'tendongsp': 'IMAC' });
        const phuKien = await loaiSanPham.find({ 'tendongsp': 'PHỤ KIỆN' });
        const sanPhamResult = await sanPham.find({});
        const data = sanPhamResult.filter(function(item) {
            return item.tensanpham.toLowerCase().indexOf(search.toLowerCase()) !== -1
        });
        res.render('Client/products', {
            macBook: macBook,
            iMac: iMac,
            phuKien: phuKien,
            user: req.user,
            sanPham: data,
            search: search
        })
    } catch (e) {
        res.send('Đường dẫn không hợp lệ')
    }
})

router.get('/autocomplete', async(req, res) => {
    try {
        const search = req.query.data
        const sanPhamResult = await sanPham.find({}).select('tensanpham');
        const result = sanPhamResult.filter(function(item, index) {
            return item.tensanpham.toLowerCase().indexOf(search.toLowerCase()) !== -1
        });
        res.send(result.slice(0, 7))
    } catch (e) {
        res.send('Đường dẫn không hợp lệ')
    }

})

router.get('/addcart/:id',isLoggedIn, (req, res) => {
	let newGioHang = new gioHang({
        idsanpham: req.params.id,
        idthanhvien: req.user._id
    });
    newGioHang.save(function(err) {
        if (err) {
            res.json({ kq: false, errMsg: err });
        } else {
            res.redirect('/cart');
        }
    })
})


//Thêm thông tin cho người dùng mới đăng ký
router.post('/thongtinkhachhang', (req, res) => {
    let newThongTinTv = new thongTinTV({
        sdt: req.body.sdt,
        gioitinh: req.body.gioitinh,
        namsinh: req.body.namsinh,
        diachi: req.body.diachi
    });
    newThongTinTv.save(function(err) {
        if (err) {
            res.json({ kq: false, errMsg: err });
        } else {
            thanhVien.findOneAndUpdate({ _id: req.user._id }, {
                    $push: {
                        idthongtin: newThongTinTv._id
                    }
                },
                function(err) {
                    if (err) {
                        res.json({ kq: false, errMsg: err });
                    } else {
                        req.flash('message', 'Success! Cập nhật thông tin thành công');
                        backURL = req.header('Referer') || '/';
                        res.redirect(backURL);
                    }
                }
            )
        }
    })
});




// Cập nhật thông tin khách hàng

router.post('/thongtinkhachhang/:id', (req, res) => {
    let id = req.params.id;
    thongTinTV.findByIdAndUpdate({ _id: id }, {
            $set: {
                hoten: req.body.hoten,
                sdt: req.body.sdt,
                gioitinh: req.body.gioitinh,
                namsinh: req.body.namsinh,
                diachi: req.body.diachi
            }
        },
        function(err, data) {
            if (err) {
                res.json({ kq: false, errMsg: err });
            } else {
                req.flash('message', 'Success! Cập nhật thông tin thành công');
                backURL = req.header('Referer') || '/';
                res.redirect(backURL);
            }
        });
});

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/login');
}

//Bình luận sản phẩm
router.post('/post-comment', (req, res) => {
    let newNhanXet = new nhanXet({
        noidung: req.body.noidung,
        idthanhvien: req.user._id,
        idsanpham: req.body.idsanpham
    });
    newNhanXet.save(function(err) {
        if (err) {
            res.json({ kq: false, errMsg: err });
        } else {
            res.send({
                newNhanXet, 
                user: req.user
            })
        }
    })
})

router.get('/deletecomment/:id', (req, res) => {
    nhanXet.findByIdAndDelete(req.params.id, function(err, data) {
        if (err) {
            res.json({ kq: false, errMsg: err });
        } else {
            req.flash('success', 'Xóa nhận xét thành công!')
            backURL = req.header('Referer') || '/';
            res.redirect(backURL);
        }
    })
})


module.exports = router;