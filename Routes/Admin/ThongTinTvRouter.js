var express = require('express');
var thanhVien = require('../../Database/models/ThanhVien');
var thongTinTv = require('../../Database/models/ThongTinThanhVien');
var Router = express.Router();

//thông tin cá nhân
Router.get('/thongtincanhan', (req, res) => {
    if (
        req.isAuthenticated() && 
        (req.user.role == 'admin' || 
        req.user.role == 'CatalogManager' || 
        req.user.role == 'OrderManager')
    ) {
        thanhVien.findById(req.user._id)
            .populate('idthongtin')
            .exec((err, thongtin) => {
                res.render('Admin/ThongTinCaNhan', {
                    user: req.user,
                    thongtin: thongtin,
                    messageUpdate: req.flash('successUpdate')
                });
            });
    } else {
        res.redirect('/admin');
    }
});

//Thêm thông tin cho người dùng mới đăng ký
Router.post('/thongtincanhan', (req, res) => {
    let newThongTinTv = new thongTinTv({
        sdt: req.body.sdt,
        gioitinh: req.body.gioitinh,
        namsinh: req.body.namsinh,
        diachi: req.body.diachi
    });
    newThongTinTv.save(function(err) {
        if (err) {
            res.json({ kq: false, errMsg: err });
        } else {
            thanhVien.findOneAndUpdate({ _id: req.user._id }, {
                    $push: {
                        idthongtin: newThongTinTv._id
                    }
                },
                function(err) {
                    if (err) {
                        res.json({ kq: false, errMsg: err });
                    } else {
                        req.flash('successUpdate', 'Success! Cập nhật thông tin thành công');
                        backURL = req.header('Referer') || '/';
                        res.redirect(backURL);
                    }
                }
            )
        }
    })
});

//Cập nhật thông tin người dùng
Router.post('/thongtincanhan/:id', (req, res) => {
    let id = req.params.id;
    console.log(req);
    thongTinTv.findByIdAndUpdate({ _id: id }, 
    {
        $set: {
            sdt: req.body.sdt,
            gioitinh: req.body.gioitinh,
            namsinh: req.body.namsinh,
            diachi: req.body.diachi
        }
    },
    function(err, data) {
        if (err) {
            res.json({ kq: false, errMsg: err });
        } else {
            req.flash('successUpdate', 'Success! Cập nhật thông tin thành công');
            backURL = req.header('Referer') || '/';
            res.redirect(backURL);
        }
    });
});

module.exports = Router;