var express = require('express');
var loaiSanPham = require('../../Database/models/LoaiSanPham');
var sanPham = require('../../Database/models/SanPham');
var multer = require('multer');
var Router = express.Router();

//Trang thêm sản phẩm
Router.get('/themsanpham', (req, res) => {
    if (req.isAuthenticated() && (req.user.role == 'admin' || req.user.role == 'CatalogManager')) {
        loaiSanPham.find({}).exec((err, lsp) => {
            res.render('Admin/ThemSanPham', {
                loaisanpham: lsp,
                messageAdd: req.flash('successAdd'),
                user: req.user
            });
        });
    } else {
        res.redirect('/admin');
    }
});

//Hiển thị danh sách sản phẩm
Router.get('/dssanpham', (req, res) => {
    if (req.isAuthenticated() && (req.user.role == 'admin' || req.user.role == 'CatalogManager')) {
        sanPham.find({})
        .populate('idloaisp')
        .exec((err, sp) => {
            res.render('Admin/DanhSachSanPham', {
                sanpham: sp,
                messageUpdate: req.flash('successUpdate'),
                messageDelete: req.flash('successDelete'),
                user: req.user
            });
        });
    } else {
        res.redirect('/admin');
    }
});

//Thêm sản phẩm
var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './public/upload')
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + "-" + file.originalname)
    }
});

var upload = multer({
    storage: storage,
    fileFilter: function(req, file, cb) {
        if (file.mimetype == "image/bmp" ||
            file.mimetype == "image/png" ||
            file.mimetype == "image/gjf" ||
            file.mimetype == "image/jpg" ||
            file.mimetype == "image/jpeg") {
            cb(null, true)
        } else {
            return cb(new Error('Only image are allowed!'))
        }
    }
}).single("hinhanh");

Router.post("/themsanpham", function(req, res) {
    upload(req, res, function(err) {
            if (err instanceof multer.MulterError) {
                console.log("A Multer error occurred when uploading.");
            } else if (err) {
                console.log("An unknown error occurred when uploading." + err);
            } else {
                let newSanPham = new sanPham({
                    tensanpham: req.body.tensanpham,
                    giatien: req.body.giatien,
                    giakhuyenmai: req.body.giakhuyenmai,
                    namsx: req.body.namsx,
                    hinhanh: req.file.filename,
                    mota: req.body.mota,
                    thongsokythuat: req.body.thongsokythuat
                });
                newSanPham.save(function(err) {
                    if (err) {
                        res.json({ kq: false, errMsg: err });
                    } else {
                        sanPham.findOneAndUpdate({ _id: newSanPham._id }, {
                                $push: {
                                    idloaisp: req.body.loaisp,
                                }
                            },
                            function(err) {
                                if (err) {
                                    res.json({ kq: false, errMsg: err });
                                } else {
                                    req.flash('successAdd', 'Success! Thêm sản phẩm thành công!')
                                    res.redirect('/admin/themsanpham');
                                }
                            }
                        );
    
                    }
                })
            }
    });
});

//Chỉnh sửa sản phẩm
Router.get('/dssanpham/update/:id', (req, res) => {
    if (req.isAuthenticated() && (req.user.role == 'admin' || req.user.role == 'CatalogManager')) {
        sanPham.findById(req.params.id, (err, sp) => {
            loaiSanPham.find({}).exec((err, lsp) => {
                res.render('Admin/CapNhatSanPham', {
                    loaisanpham: lsp,
                    sanpham: sp,
                    user: req.user
                });
            });
        });
    } else {
        res.redirect('/admin');
    }
});

Router.post('/dssanpham/update/:id', (req, res) => {
    let idSanPham = req.params.id;
    sanPham.findByIdAndUpdate({ _id: idSanPham }, {
            $set: {
                tensanpham: req.body.tensanpham,
                giatien: req.body.giatien,
                giakhuyenmai: req.body.giakhuyenmai,
                namsx: req.body.namsx,
                mota: req.body.mota,
                thongsokythuat: req.body.thongsokythuat,
                idloaisp: req.body.loaisp,
            }
        },
        function(err, data) {
            if (err) {
                res.json({ kq: false, errMsg: err });
            } else {
                req.flash('successUpdate', 'Success! Cập nhật sản phẩm thành công!');
                res.redirect('/admin/dssanpham');
            }
        });
})

//Xóa sản phẩm
Router.get('/dssanpham/delete/:id', (req, res) => {
    if (req.isAuthenticated() && (req.user.role == 'admin' || req.user.role == 'CatalogManager')) {
        sanPham.findByIdAndDelete(req.params.id, function(err, data) {
            if (err) {
                res.json({ kq: false, errMsg: err });
            } else {
                req.flash('successDelete', 'Success! Xóa sản phẩm thành công!')
                backURL = req.header('Referer') || '/';
                res.redirect(backURL);
            }
        })
    } else {
        res.redirect('/admin');
    }
});
module.exports = Router;