var express = require('express');
var loaiSanPham = require('../../Database/models/LoaiSanPham');
var sanPham = require('../../Database/models/SanPham');
var router = express.Router();

//loại sản phẩm

router.get('/dongsanpham', async(req, res) => {
    if (req.isAuthenticated() && (req.user.role == 'admin' || req.user.role == 'CatalogManager')) {
        try {
            const macBook = await loaiSanPham.find({'tendongsp': 'MACBOOK'});
            const imac = await loaiSanPham.find({'tendongsp': 'IMAC'});
            const phuKien = await loaiSanPham.find({'tendongsp': 'PHỤ KIỆN'});
            const update = await loaiSanPham.findById(req.query.update);

            return res.render('Admin/DongSanPham', {
                macbook: macBook,
                imac: imac,
                phukien: phuKien,
                user: req.user,
                update: update,
                message: req.flash('success'),
                messageFail: req.flash('fail')
            });

        } catch (err) {
            console.log(err)
        }
    } else {
        res.redirect('/admin');
    }
});
//Thêm loại sản phẩm
router.post('/dongsanpham', (req, res) => {
    let newLoaiSanPham = new loaiSanPham({
        tenloaisp: req.body.tenloaisp,
        tendongsp: req.body.tendongsp
    });
    newLoaiSanPham.save(function(err) {
        if (err) {
            res.json({ kq: false, errMsg: err });
        } else {
            req.flash('success', 'Success! Thêm loại sản phẩm thành công')
            res.redirect('/admin/dongsanpham');
        }
    })
})

//Cập nhật loại sản phẩm
router.post('/dongsanpham/:id', (req, res) => {
    let id = req.params.id;
    loaiSanPham.findByIdAndUpdate({ _id: id }, 
    {
        $set: {
            tenloaisp: req.body.tenloaisp,
            tendongsp: req.body.tendongsp
        }
    },
    function(err, data) {
        if (err) {
            res.json({ kq: false, errMsg: err });
        } else {
            req.flash('success', 'Success! Cập nhật loại sản phẩm thành công')
            res.redirect('/admin/dongsanpham');
        }
    });
});

//Xóa loại sản phẩm
router.get("/dongsanpham/delete/:id", function(req, res) {
    if (req.isAuthenticated() && (req.user.role == 'admin' || req.user.role == 'CatalogManager')) {
        sanPham.find({}).exec((err, sp) => {
            let count = 0
            sp.forEach(function(data) {
                if (req.params.id == data.idloaisp) {
                    count++
                }
            })
            if (count == 0) {
                loaiSanPham.findByIdAndDelete(req.params.id, function(err, data) {
                    if (err) {
                        res.json({ kq: false, errMsg: err });
                    } else {
                        req.flash('success', 'Success! Xóa loại sản phẩm thành công!')
                        backURL = req.header('Referer') || '/';
                        res.redirect(backURL);
                    }
                })
            } else {
                req.flash('fail', 'Fail! Có nhiểu sản phẩm thuộc loại sản phẩm này, không thể xóa. Vui lòng thử lại!')
                backURL = req.header('Referer') || '/';
                res.redirect(backURL);
            }
        })
    } else {
        res.redirect('/admin');
    }

});
module.exports = router;