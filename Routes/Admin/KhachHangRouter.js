var express = require('express');
var thanhVien = require('../../Database/models/ThanhVien');
var router = express.Router();

router.get('/khachhang', (req, res) => {
    if (req.isAuthenticated() && req.user.role == 'admin') {
        thanhVien.find({ role: 'Customer' })
        .populate('idthongtin')
        .exec((err, tv) => {
            res.render('Admin/KhachHang', {
                thanhvien: tv,
                user: req.user,
                messageDelete: req.flash('successDelete')
            });
        });
    } else {
        res.redirect('/admin');
    }
});
module.exports = router;