var express = require('express');
var thanhVien = require('../../Database/models/ThanhVien');
var thongTinTv = require('../../Database/models/ThongTinThanhVien');
var bcrypt = require('bcryptjs');
var saltRounds = 10;
var router = express.Router();

//Hiển thị danh sách nhân viên
router.get('/',  (req, res) => {
    if (req.isAuthenticated() && req.user.role == 'admin') {
        thanhVien.find({ $or: [{ role: 'OrderManager' }, { role: 'CatalogManager' }] })
            .populate('idthongtin')
            .exec((err, tv) => {
                res.render('Admin/Index', {
                    thanhvien: tv,
                    user: req.user,
                    messageDelete: req.flash('successDelete')
                });
            })
    }else if(req.isAuthenticated() && req.user.role == 'CatalogManager') {
        res.redirect('/admin/dssanpham')
    }else if(req.isAuthenticated() && req.user.role == 'OrderManager'){
        res.redirect('/admin/thongtincanhan')
    }
    else {
        res.redirect('/admin/login');
    }
});
//Xóa thành viên
router.get('/thanhvien/delete', (req, res) => {
    if (req.isAuthenticated() && req.user.role == 'admin') {
        let tv = req.query.tv;
        let tttv = req.query.tttv;
        thanhVien.findByIdAndDelete(tv, function(err, data) {
            thongTinTv.findByIdAndDelete(tttv, function(err, data) {
                req.flash('successDelete', 'Success! Xóa người dùng thành công!')
                backURL = req.header('Referer') || '/';
                res.redirect(backURL);
            })
        })
    } else {
        res.redirect('/admin');
    }
})

//Thêm nhân viên mới
router.get('/themnhanvien', (req, res) => {
    if (req.isAuthenticated() && req.user.role == 'admin') {
        res.render('Admin/ThemNhanVien', {
            user: req.user,
            messageAdd: req.flash('successAdd'),
            messageFailAdd: req.flash('failAdd'),
        })
    } else {
        res.redirect('/admin');
    }
});

router.post('/themnhanvien', (req, res) => {
    thanhVien.findOne({email: req.body.email}, (err, acc) => {
        if(acc) {
            req.flash('failAdd', 'Fail! Email đã được sử dụng!')
            backURL = req.header('Referer') || '/';
            res.redirect(backURL);
        }else {
            bcrypt.hash(req.body.password, saltRounds, function(err, hash) {
                thanhVien.create({
                    hoten: req.body.hoten,
                    email: req.body.email,
                    role: req.body.role,
                    password: hash
                }).then(function(data) {
                    if (data) {
                        req.flash('successAdd', 'Success! Thêm nhân viên thành công!')
                        backURL = req.header('Referer') || '/';
                        res.redirect(backURL);
                    }
                });
            });
        }
    })
    
})
module.exports = router;