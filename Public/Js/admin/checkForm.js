function validate() {
    var tensanpham = document.themsanpham.tensanpham.value;
    var giatien = document.themsanpham.giatien.value;
    var mota = document.themsanpham.mota.value;
    var hinhanh = document.themsanpham.hinhanh.value;
    var loaisp = document.themsanpham.loaisp.value;
    var giakhuyenmai = document.themsanpham.giakhuyenmai.value;
    var status = false;

    if (tensanpham.length < 1) {
        document.getElementById("errorTensanpham").innerHTML = "Vui lòng nhập tên sản phẩm";
        status = false;
    } else {
        document.getElementById("errorTensanpham").innerHTML = "";
        status = true;
    }

    if (giakhuyenmai < 0) {
        document.getElementById("errorGiakhuyenmai").innerHTML = "Giá khuyến mãi không được là số âm";
        status = false;
    } else if (Number(giakhuyenmai) > Number(giatien)) {
        document.getElementById("errorGiakhuyenmai").innerHTML = "Giá khuyến mãi không thể lớn hơn giá gốc";
        status = false;
    } else {
        document.getElementById("errorGiakhuyenmai").innerHTML = "";
    }

    if (giatien.length < 1) {
        document.getElementById("errorGiatien").innerHTML = "Vui lòng nhập giá tiền";
        status = false;
    } else if (giatien < 0) {
        document.getElementById("errorGiatien").innerHTML = "Giá tiền không được là số âm";
        status = false;
    } else {
        document.getElementById("errorGiatien").innerHTML = "";
    }

    if (loaisp.length < 1) {
        document.getElementById("errorLoaisp").innerHTML = "Vui lòng chọn loại sản phẩm";
        status = false;
    } else {
        document.getElementById("errorLoaisp").innerHTML = "";
    }

    if (mota.length < 1) {
        document.getElementById("errorMota").innerHTML = "Vui lòng nhập mô tả";
        status = false;
    } else {
        document.getElementById("errorMota").innerHTML = "";
    }

    if (hinhanh.length < 1) {
        document.getElementById("errorHinhanh").innerHTML = "Vui lòng chọn ảnh";
        status = false;
    } else {
        document.getElementById("errorHinhanh").innerHTML = "";
    }

    return status;
}

function validateUpdate() {
    var tensanpham = document.themsanpham.tensanpham.value;
    var giatien = document.themsanpham.giatien.value;
    var mota = document.themsanpham.mota.value;
    var loaisp = document.themsanpham.loaisp.value;
    var giakhuyenmai = document.themsanpham.giakhuyenmai.value;
    var status = false;
    console.log(loaisp)
    if (tensanpham.length < 1) {
        document.getElementById("errorTensanpham").innerHTML = "Vui lòng nhập tên sản phẩm";
        status = false;
    } else {
        document.getElementById("errorTensanpham").innerHTML = "";
        status = true;
    }

    if (giakhuyenmai < 0) {
        document.getElementById("errorGiakhuyenmai").innerHTML = "Giá khuyến mãi không được là số âm";
        status = false;
    } else if (Number(giakhuyenmai) > Number(giatien)) {
        document.getElementById("errorGiakhuyenmai").innerHTML = "Giá khuyến mãi không thể lớn hơn giá gốc";
        status = false;
    } else {
        document.getElementById("errorGiakhuyenmai").innerHTML = "";
    }

    if (giatien.length < 1) {
        document.getElementById("errorGiatien").innerHTML = "Vui lòng nhập giá tiền";
        status = false;
    } else if (giatien < 0) {
        document.getElementById("errorGiatien").innerHTML = "Giá tiền không được là số âm";
        status = false;
    } else {
        document.getElementById("errorGiatien").innerHTML = "";
    }

    if (mota.length < 1) {
        document.getElementById("errorMota").innerHTML = "Vui lòng nhập mô tả";
        status = false;
    } else {
        document.getElementById("errorMota").innerHTML = "";
    }

    if (mota.length < 1) {
        document.getElementById("errorMota").innerHTML = "Vui lòng nhập mô tả";
        status = false;
    } else {
        document.getElementById("errorMota").innerHTML = "";
    }
    return status;
}