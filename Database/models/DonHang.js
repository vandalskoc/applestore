var mongoose = require('mongoose');

var donhangSchema = mongoose.Schema({
    idsanpham: [{ type: mongoose.Types.ObjectId, ref: 'sanpham' }],
    idthanhvien: { type: mongoose.Types.ObjectId, ref: 'thanhvien' },
    soluong: [{type: Number, default: 1}],
    thoigian: {type: Date, default: Date.now()},
    thanhtien: Number
});

module.exports = mongoose.model('donhang', donhangSchema, 'donhang');