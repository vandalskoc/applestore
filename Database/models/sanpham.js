var mongoose = require('mongoose');

var sanphamSchema = mongoose.Schema({
    tensanpham: String,
    giatien: Number,
    // giakhuyenmai: Number,
    hinhanh: String,
    mota: String,
    thongsokythuat: String,
    idloaisp: { type: mongoose.Types.ObjectId, ref: 'loaisanpham' },
    luotmua: { type: Number, default: 0 }
});

module.exports = mongoose.model('sanpham', sanphamSchema, 'sanpham');