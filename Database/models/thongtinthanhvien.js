var mongoose = require('mongoose');

var thongtintvSchema = mongoose.Schema({
    sdt: String,
    gioitinh: String,
    namsinh: String,
    hinhanhtv: String,
    diachi: String
});

module.exports = mongoose.model('thongtintv', thongtintvSchema, 'thongtintv');