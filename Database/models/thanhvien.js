var mongoose = require('mongoose');

var userSchema = mongoose.Schema({
    uid: String,
    email: {type: String, unique: true},
    hoten: String,
    password: String,
    idthongtin: { type: mongoose.Types.ObjectId, ref: 'thongtintv' },
    role: {type: String, default: 'Customer'}
});

module.exports = mongoose.model('thanhvien', userSchema, 'thanhvien');