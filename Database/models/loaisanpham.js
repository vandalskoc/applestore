var mongoose = require('mongoose');

var loaisanphamSchema = mongoose.Schema({
    tenloaisp: String,
    tendongsp: String
});

module.exports = mongoose.model('loaisanpham', loaisanphamSchema, 'loaisanpham');