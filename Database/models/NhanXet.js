var mongoose = require('mongoose');

var nhanxetSchema = mongoose.Schema({
    noidung: String,
    thoigian: {type: Date, default: Date.now()},
    idsanpham: { type: mongoose.Types.ObjectId, ref: 'sanpham' },
    idthanhvien: { type: mongoose.Types.ObjectId, ref: 'thanhvien' },
});

module.exports = mongoose.model('nhanxet', nhanxetSchema, 'nhanxet');