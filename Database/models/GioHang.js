var mongoose = require('mongoose');

var giohangSchema = mongoose.Schema({
    idsanpham: { type: mongoose.Types.ObjectId, ref: 'sanpham' },
    idthanhvien: { type: mongoose.Types.ObjectId, ref: 'thanhvien' },
    soluong: {type: Number, default: 1}
});

module.exports = mongoose.model('giohang', giohangSchema, 'giohang');